// All material copyright ESRI, All Rights Reserved, unless otherwise specified.
// See http://arcgis.esrivn.net/portal/jsapi/jsapi/esri/copyright.txt and http://www.arcgis.com/apps/webappbuilder/copyright.txt for details.

//>>built
define([
        "dojo/_base/declare",
        "jimu/BaseWidget",
		"esri/map",
		"esri/toolbars/draw",
		"esri/graphic",

		"esri/graphicsUtils",
		"esri/symbols/SimpleMarkerSymbol",
		"esri/symbols/SimpleLineSymbol",
		"esri/symbols/SimpleFillSymbol",
		"esri/Color",

		"esri/tasks/Geoprocessor",
		"esri/tasks/FeatureSet",
		"esri/tasks/LinearUnit",
		"dojo/ready",
		"dojo/parser",

		"dojo/on",
		"dojo/_base/array",
		"esri/tasks/QueryTask",
		"esri/tasks/query",
		"esri/arcgis/utils",

		"esri/symbols/SimpleMarkerSymbol",
		"dijit/registry",
        "dijit/Menu",
        "dijit/MenuItem",
		"esri/request",

		"dojox/gfx/fx",
		"dojox/charting/Chart",
		"dojox/charting/plot2d/Pie",
		"dojox/charting/action2d/Highlight",
		"dojox/charting/action2d/MoveSlice",

		"dojox/charting/action2d/Tooltip",
		"dojox/charting/themes/MiamiNice",
		"dojox/charting/widget/Legend",
		"dijit/Dialog",
		"dijit/form/TextBox",

		"dijit/form/Button",
		"esri/layers/StreamLayer",
         "dojo/dom",
		"esri/InfoTemplate",
         "esri/tasks/GeometryService",
         "esri/tasks/BufferParameters",
          "esri/layers/FeatureLayer",
         "esri/config", "dojo/_base/Color"

],
    function (
        b,
        c,
		Map,
		Draw,
		Graphic,

		graphicsUtils,
		SimpleMarkerSymbol,
		SimpleLineSymbol,
		SimpleFillSymbol,
		Color,

		Geoprocessor,
		FeatureSet,
		LinearUnit,
		ready,
		parser,

		on,
		array,
		QueryTask,
		Query,
		arcgisUtils,

		SimpleMarkerSymbol,
		registry,
        Menu,
		MenuItem,
		esriRequest,

		dojox_gfx_fx,
		Chart,
		Pie,
		Highlight,
		MoveSlice,

		Tooltip,
		MiamiNice,
		Legend,
		Dialog,
		TextBox,

		Button,
		StreamLayer,
        dom,
		InfoTemplate,
        GeometryService,
        BufferParameters,
        FeatureLayer,
         esriConfig, Color
    ) {
        return b([c], {
            baseClass: 'jimu-widget-demo',
            postCreate: function () {
                this.inherited(arguments);
                console.log("postCreate")

                that1 = this;
                that1.PHANLOAI = [];
                that1.geotram = [];
                that1.graphic;
                that1.PHUONGAN = [];
                that1.MAPS = [];
                that1.ToaDoX = 0;
                that1.ToaDoY = 0;
                that1.TramDaGiaoViecBD = 0;
                that1.ZoomTramSuCo = {};
                that1.ZoomTramGiaoViec = {};
                that1.SoLuongTramSuCo = 0;
                that1.SoLuongTramChuaGiaoViec = 0;
                that1.SoLuongTramDaGiaoViec = 0;
                that1.SoLuongTramDaXuLyXong = 0;
                that1.SoLuongTramDangXuly = 0;
                that1.SoLuongTramHuy = 0;
                that1.DanhSachTramTrongBangGV = 0;
                that1.mauHienThi = "red";
                that1.KiemTraTrangThaiLietKeDiemGiaoViecTrenBanDo = 1;
                that1.KiemTraTrangThaiLietKeTramSuCoTrenBanDo = 0;
                that1.PointTramDaXuLyXong = [];
                that1.PointTramDaGiaoViec = [];
                that1.PointTramChuaGiaoViec = [];
                that1.MyDialog;
                that1.LOOKUP = {};
                that1.urlFearturePhuongan = "https://gis1061.esrivn.net/server/rest/services/PCCC/Call_info/FeatureServer";
                //that1.DialogPhamViAnhHuong;
                //phân loại đám cháy
                that1.PHANLOAI[1] = "Cháy nhà dân mức độ 1";
                that1.PHANLOAI[2] = "Cháy nhà dân mức độ 2";
                that1.PHANLOAI[3] = "Cháy Trung tâm thương mại mức độ 1";
                that1.PHANLOAI[4] = "Cháy Trung tâm thương mại mức độ 2";
                that1.PHANLOAI[5] = "Cháy nhà cao tầng mức độ 1";
                that1.PHANLOAI[6] = "Cháy nhà cao tầng mức độ 2";
                //phương án đề xuất

                that1.PHUONGAN[1] = "01 Xe bơm có téc \n01 Xe trở quân";
                that1.PHUONGAN[2] = "02 Xe bơm có téc\n01 Xe trở quân";
                that1.PHUONGAN[3] = "01 Xe bơm có téc\n01 Xe thang \n 01 xe trở quân\n01 Xe chỉ huy ";
                that1.PHUONGAN[4] = "02 Xe bơm có téc\n01 Xe trở quân\n01 Xe chỉ huy";
                that1.PHUONGAN[5] = "02 Xe có téc\n02 Xe thang vươn thẳng\n01 Xe trở quân\n01 Xe chỉ huy";
                that1.PHUONGAN[6] = "02 Xe có téc\n04 Xe thang vươn thẳng<br>	02 Xe trở quân\n01 Xe chỉ huy";

                that1.geometryService = new GeometryService("https://gis1061.esrivn.net/server/rest/services/Utilities/Geometry/GeometryServer");
                setInterval(
                  () => { 
                      that1.KiemTraTramDien();
                  //   that1.getScrash();
                  },
                  20000
                );
                that1.KiemTraTramSuCo();
                that1.markerSymbol = {
                    type: "simple-marker",
                    color: [226, 119, 40],
                    outline: {
                        color: [255, 255, 255],
                        width: 2
                    }
                };
                that1.featureLayer = new FeatureLayer("https://gis1061.esrivn.net/server/rest/services/PCCC/DoiPCCC/FeatureServer/0", {
                    mode: FeatureLayer.MODE_SELECTION,
                    outFields: ['*']
                });
                that1.MAPS[0] = this.map;
                that1.MAPS[0].disableDoubleClickZoom();
                that1.MAPS[0].on("dbl-click", function (evt) {
                    var params = new BufferParameters();
                    params.geometries = [evt.mapPoint];
                    params.distances = [2];
                    params.unit = GeometryService.UNIT_STATUTE_MILE;

                    that1.geometryService.buffer(params);
                });
                that1.MAPS[0].on("click", function (evt) {
                    that1.MAPS[0].graphics.clear();
                });


                that1.geometryService.on("buffer-complete", function (result) {
                    that1.MAPS[0].graphics.clear();
                    console.log('ac');
                    var sym = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SHORTDASHDOTDOT, new Color([255, 0, 0]), 5), new Color([255, 222, 0, 0.25]));
                    var buffer = result.geometries[0]
                    var graphic = new Graphic(buffer, sym);
                    that1.MAPS[0].graphics.add(graphic);

                    var query = new Query();
                    query.geometry = buffer;
                    that1.cbochondoi.innerHTML = "<option value='all'>--chọn đội phòng cháy--</option>";

                    that1.featureLayer.selectFeatures(query, FeatureLayer.SELECTION_NEW, function (results) {

                        var total = results.length;
                        var tb = "";
                        tb = "<b>Tổng số đối tượng là: <i>" + total + "</i>.</b>";
                        for (var i = 0; i < results.length; i++) {
                            var attr = results[i].attributes;
                            var opt = document.createElement("option");
                            opt.value = attr["Code"];
                            opt.innerHTML = attr["Name"];

                            that1.geotram[attr["Code"]] = results[i].geometry;
                            //this.tag = results[i].geometry;
                            //opt.onchange = that1.zoomtoitram;
                            that1.cbochondoi.add(opt);
                        }
                    });
                });
                that1.point = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 25, new SimpleLineSymbol(SimpleLineSymbol.STYLE_NULL, new Color([137, 214, 171, 0.9]), 1), new Color([137, 214, 171, 0.5]));
                //that1.addStreamLayer2();
                //that1.addStreamLayer();

                this.cbochondoi.onchange = this.zoomtoitram;
            },

       
            

         

        
            zoomtoitram: function () {
                if (that1.graphic) {
                    that1.MAPS[0].graphics.remove(that1.graphic);

                }
                that1.MAPS[0].graphics.clear();
                //alert(that1.cbochondoi.value);
                var symbol = new esri.symbol.SimpleMarkerSymbol();
                symbol.style = esri.symbol.SimpleMarkerSymbol.STYLE_SQUARE;
                symbol.setSize(10); symbol.setColor(new dojo.Color([255, 255, 0, 0.5]));

                that1.graphic = new Graphic({
                    geometry: that1.geotram[that1.cbochondoi.value],
                    symbol: symbol
                });
                that1.graphic.setSymbol(symbol);
                that1.MAPS[0].graphics.add(that1.graphic);
            },
            KiemTraTramSuCo: function () {

                // dua cac tham so ve bieu do ve dang mac dinh
                that1.SoLuongTramSuCo = 0;

                var urlServices = "https://gis1061.esrivn.net/server/rest/services/PCCC/Call_info/FeatureServer/0";

                var query = new Query();
                query.outFields = ["*"];
                //query.orderByFields = ["DATE DESC"];
                query.returnGeometry = true;
                query.where = '1=1';

                var task = new QueryTask(urlServices);
                task.execute(query, function (set) {

                    //if(that1.KiemTraTrangThaiLietKeTramSuCoTrenBanDo == 0){
                    //	// set tham so de zoom to toi tram su co
                    //	that1.TramDaGiaoViecBD = set.features;
                    //	if (that1.MAPS.length > 0) {
                    //		that1.ThemTramDaGiaoViecTrenBanDo(that1.MAPS[0],that1.TramDaGiaoViecBD, 'red');
                    //	}
                    that1.tblT1.innerHTML = "";
                    //	// dem so tram su co
                    //	that1.SoLuongTramSuCo = set.features.length;
                    for (var i = 0; i < set.features.length; i++) {
                        var attr = set.features[i].attributes;
                        // set tham so de zoom to toi tram su co
                        that1.ZoomTramSuCo[attr["OBJECTID"]] = set.features[i].geometry;
                        var row = that1.tblT1.insertRow();
                        row.className = 'line';
                        row.innerHTML = "<td>" + (i + 1) + "</td><td>" + that1.PHANLOAI[attr["PHANLOAI"]] + "</td><td>" + attr["SODIENTHOAI"] + "</td><td>" + attr["MOTA"] + "</td><td hidden='hidden'>" + attr["PHANLOAI"] + "</td><td hidden='hidden'>" + attr["OBJECTID"] + "</td>";
                        //row.onclick=that1.row_onclick;
                        row.onclick = that1.ZoomToTramSuCo;
                        row.tag = set.features[i].geometry;
                        row.align = "center";
                        row.cells[0].tag = i + 1;
                        row.cells[1].tag = attr["PHANLOAI"];
                        row.cells[2].tag = attr["SODIENTHOAI"];
                        row.cells[3].tag = attr["MOTA"];
                        row.cells[4].tag = attr["PHANLOAI"];
                    }

                     

                }, function (err) {
                    //alert(err.stack);
                });
                //load phương án
                that2.tblphuongan.tBodies[0].innerHTML = "";
                var query = new Query();

                query.outFields = ["*"];
                query.orderByFields = ["OBJECTID"];
                query.returnGeometry = false;
                query.where = "1=1";
                var task = new QueryTask("https://gis1061.esrivn.net/server/rest/services/PCCC/Call_info/FeatureServer/1");
                task.execute(query, function (set) {
                    for (var i = 0; i < set.features.length; i++) {
                        var attr = set.features[i].attributes;
                        var trangthai = attr["TRANGTHAI_PHANHOI"] == 0 ? "Đang chờ" : "Đã chấp nhận";
                        var row = that2.tblphuongan.tBodies[0].insertRow();
                        row.align = "center";
                        row.innerHTML = "<td>" + (i + 1) + "</td><td  >" + attr["MOTA_DAMCHAY"] + "</td> <td> " + attr["PHUONGANDEXUAT"] + "</td> <td> " + attr["DONVINHAN"] + "</td> <td> " + attr["PHUONGANPHANHOI"] + " </td>  <td> " + (trangthai) + "</td><td hidden ='hidden'> " + attr["DAMCHAYID"] + " </td>";
                        row.onclick = that2.row_onclick;

                    }
                }, function (err) {
                    alert(err.stack);
                });
            },
            ZoomToTramSuCo: function () {

                var geo = this.tag;
                that1.MAPS[0].centerAndZoom(geo, 15);
                // thiết lập phương án đề xuất
                var html = "";
                html = that1.PHUONGAN[this.children[4].innerText];
                that1.txtphuongan.innerHTML = html;
                that1.txtiddamchay = this.children[5].innerText;
                that1.txtmotadamchay = this.children[3].innerText;
                // var  node = dom.byId('iddexuat');
                //this.setNodeHTML(node, html);
                //buffer hiển thị danh sách trạm gần nhất

            },
 

            KiemTraTramDien: function () {
                //console.log('Hello anh em every 1 seconds')
                //if (that1.KiemTraTrangThaiLietKeTramSuCoTrenBanDo == 0) {
                    // cac thong tin can duoc cap nhat thuong xuyen
                  //  that1.KiemTraTrangThaiGiaoViec();
                    that1.KiemTraTramSuCo();
                    // lay danh sach nhan cong
                    //that1.LayDanhSachNhanCong();
                    //that1.LayDanhSachCar();
                //}
            },

           
 

            // xap sep tram
            sortTram: function (index, col, isStr) {
                for (var i = 0; i < col.parentNode.children.length; i++)
                    col.parentNode.children[i].lastElementChild.innerHTML = " ↑↓";
                col.lastElementChild.innerHTML = (col.isAsc) ? " ↓" : " ↑";

                var table = col.parentNode.parentNode.nextElementSibling;
                var ok = true;
                while (ok) {
                    ok = false;
                    for (var i = 1; i < table.rows.length; i++) {
                        var vali = (isStr) ? table.rows[i].cells[index].innerHTML : table.rows[i].cells[index].tag;
                        var vali_1 = (isStr) ? table.rows[i - 1].cells[index].innerHTML : table.rows[i - 1].cells[index].tag;
                        if ((col.isAsc && vali < vali_1) || (!col.isAsc && vali > vali_1)) {
                            table.rows[i - 1].parentNode.insertBefore(table.rows[i], table.rows[i - 1]);
                            ok = true;
                        }
                    }
                }
                col.isAsc = !col.isAsc;
                // danh lai so thu tu cho bang
                /*
				for(var i=0;i < table.childNodes.length;i++){
					table.childNodes[i].firstElementChild.innerHTML = i + 1 ;
				}
				*/
            },

            startup: function () {
                this.inherited(arguments);
                //this.mapIdNode.innerHTML = "map id:" + this.map.id;
                console.log("startup")
            },
            onOpen: function () {
                //var p = this.getPanel();
                //p.setPosition({  width: "500px" });
            },
            onClose: function () {
                console.log("onClose")
            },
            onMinimize: function () {
                console.log("onMinimize")
            },
            onMaximize: function () {
                console.log("onMaximize")
            },
            onSignIn: function (a) {
                console.log("onSignIn")
            },
            onSignOut: function () {
                console.log("onSignOut")
            },
            showVertexCount: function (a) {
                this.vertexCount.innerHTML = "The vertex count is: " + a
            },
            //lấy danh sách phương án của các sự cố
            //getScrash: function()
            //{
               

            //},
            // gửi phương án hành động
            acception: function () {
                var iddamchay, idchitietdamchay, madonvinhan, tendonvinhan, phuongandexuat;
                // thêm mới phương án cứu hỏa
                iddamchay = that1.txtiddamchay;
                idchitietdamchay = that1.txtmotadamchay;
                madonvinhan = that1.cbochondoi.value;
                tendonvinhan = that1.cbochondoi.selectedOptions[0].innerText;
                phuongandexuat = that1.txtphuongan.innerHTML;
                // thêm mới phương án
                var objPhuongAn = [{
                     
                    "attributes": {
                        "MOTA_DAMCHAY": idchitietdamchay,
                        "PHUONGANDEXUAT": phuongandexuat,
                        "DONVIGUI": "c",
                        "DONVINHAN": tendonvinhan,
                        "PHUONGANPHANHOI": "e",
                        "TRANGTHAI_PHANHOI": 0,
                        "NGAYTAO": 1577750400000,
                        "NGAYCAPNHAT": 1577750400000,
                        "DAMCHAYID": iddamchay,
                        "MADONVINHAN": madonvinhan,
                        "PHUONGTIEN": "1;2"
                    }
                      
                }]
                var request = esriRequest({
                    url: that1.urlFearturePhuongan + "/1/addFeatures",
                    content: {
                        f: "json",
                        features: JSON.stringify(objPhuongAn)
                    }
                }, { usePost: true }).then(function (resp) {
                    console.log(resp);
                    if (resp.addResults[0].success == true)
                    {
                  //    alert("Gán thành công!")
                        var row = that2.tblphuongan.tBodies[0].insertRow();
                        row.align = "center";
                        row.innerHTML = "<td>" + (that2.tblchitietphuongan.childNodes.length) + "</td><td  >" + that1.txtmotadamchay+ "</td> <td> " + that1.txtphuongan.innerHTML + "</td> <td> " + that1.cbochondoi.selectedOptions[0].innerText + "</td> <td>  </td> <td> Đang chờ</td> ";
                        row.onclick = that2.row_onclick;
                    }
                }, function (err) {
                    alert(err)
                });

            }
        })
    });