// All material copyright ESRI, All Rights Reserved, unless otherwise specified.
// See http://arcgis.esrivn.net/portal/jsapi/jsapi/esri/copyright.txt and http://www.arcgis.com/apps/webappbuilder/copyright.txt for details.

//>>built
define([
        "dojo/_base/declare",
        "jimu/BaseWidget",
		"esri/map",
		"esri/toolbars/draw",
		"esri/graphic",
		
		"esri/graphicsUtils",
		"esri/symbols/SimpleMarkerSymbol",
		"esri/symbols/SimpleLineSymbol",
		"esri/symbols/SimpleFillSymbol",
		"esri/Color",

		"esri/tasks/Geoprocessor",
		"esri/tasks/FeatureSet",
		"esri/tasks/LinearUnit",
		"dojo/ready",
		"dojo/parser",
		
		"dojo/on",
		"dojo/_base/array",
		"esri/tasks/QueryTask",
		"esri/tasks/query",
		"esri/arcgis/utils",
		
		"esri/symbols/SimpleMarkerSymbol",
		"dijit/registry",
        "dijit/Menu",
        "dijit/MenuItem",
		"esri/request",
		
		"dojox/gfx/fx",
		"dojox/charting/Chart",
		"dojox/charting/plot2d/Pie",
		"dojox/charting/action2d/Highlight",
		"dojox/charting/action2d/MoveSlice",
		
		"dojox/charting/action2d/Tooltip",
		"dojox/charting/themes/MiamiNice",
		"dojox/charting/widget/Legend",
		"dijit/Dialog", 
		"dijit/form/TextBox", 
		
		"dijit/form/Button",
		"esri/layers/StreamLayer",
		"esri/InfoTemplate"
    ],
    function(
        b,
        c,
		Map,
		Draw,
		Graphic,
		
		graphicsUtils,
		SimpleMarkerSymbol,
		SimpleLineSymbol,
		SimpleFillSymbol,
		Color,
		
		Geoprocessor,
		FeatureSet,
		LinearUnit,
		ready,
		parser,
		
		on,
		array,
		QueryTask,
		Query,
		arcgisUtils,
		
		SimpleMarkerSymbol,
		registry,
        Menu,
		MenuItem,
		esriRequest,
		
		dojox_gfx_fx,
		Chart,
		Pie,
		Highlight,
		MoveSlice,
		
		Tooltip,
		MiamiNice,
		Legend,
		Dialog,
		TextBox,
		
		Button,
		StreamLayer,
		InfoTemplate
    ) {
        return b([c], {
            baseClass: 'jimu-widget-demo',
            postCreate: function() {
					this.inherited(arguments);
					console.log("postCreate")
					
					that1=this;
					that1.MAPS = [];
					that1.ToaDoX = 0;
					that1.ToaDoY = 0;
					that1.TramDaGiaoViecBD = 0;
					that1.ZoomTramSuCo = {};
					that1.ZoomTramGiaoViec = {};
					that1.SoLuongTramSuCo = 0;
					that1.SoLuongTramChuaGiaoViec = 0;
					that1.SoLuongTramDaGiaoViec = 0;
					that1.SoLuongTramDaXuLyXong = 0;
					that1.DanhSachTramTrongBangGV = 0;
					that1.mauHienThi = "red";
					that1.KiemTraTrangThaiLietKeDiemGiaoViecTrenBanDo = 1;
					that1.KiemTraTrangThaiLietKeTramSuCoTrenBanDo = 0;
					that1.PointTramDaXuLyXong = [];
					that1.PointTramDaGiaoViec = [];
					that1.PointTramChuaGiaoViec = [];
					that1.MyDialog;
					//that1.DialogPhamViAnhHuong;
					
					setInterval(
					  () => { that1.KiemTraTramDien() },
					  60000
					);
					
					// cac thong tin can duoc cap nhat thuong xuyen
					that1.KiemTraTrangThaiGiaoViec();
					that1.KiemTraTramSuCo();
					// lay danh sach nhan cong
					that1.LayDanhSachNhanCong();
					
					// hien thi ban do mang luoi dien
					that1.LayBanDoMangLuoi();
					
					that1.MAPS[0] = this.map;
					
					//that1.addStreamLayer2();
					//that1.addStreamLayer();
            },
			
			LayDanhSachCar: function(){
				var lyr2 = this.map.getLayer(this.map.graphicsLayerIds[7]).getLatestObservations();
				var lyr = this.map.getLayer(this.map.graphicsLayerIds[5]);
			},
			
		    addStreamLayer: function(){
				//url to stream service
				//var svcUrl = "https://geoeventsample1.esri.com:6443/arcgis/rest/services/LABus/StreamServer/subscribe";

			  var layerUrl = "https://geoeventsample1.esri.com:6443/arcgis/rest/services/LABus/StreamServer/subscribe";
			  var layersRequest = esriRequest({
				url: layerUrl,
				handleAs: "jsonp",
				callbackParamName: "callback"
			  });
			  layersRequest.then(
				function(response) {
				  console.log("Success: ", response.layers);
			  }, function(error) {
				  console.log("Error: ", error.message);
			  });
				
		    },
			
			addStreamLayer2: function(){
			   //url to stream service
				var svcUrl = "https://geoeventsample1.esri.com:6443/arcgis/rest/services/LABus/StreamServer/subscribe";

				//construct Stream Layer
				var streamLayer = new StreamLayer(svcUrl, {
					purgeOptions: { displayCount: 10000 },
					infoTemplate: new InfoTemplate("Attributes", "${*}")
				});
			},
			
			VeBieuDoPhamViAnhHuong: function(TongSoLuong,BiAnhHuong, TieuDe){
				that1.BieuDoTronTacDong.innerHTML = "";
				//that1.ChuThich.innerHTML = "";
				var chartTwo = new Chart(that1.BieuDoTronTacDong);
				chartTwo.setTheme(MiamiNice)
					.addPlot("default", {
						type: Pie,
						font: "normal normal 11pt Tahoma",
						fontColor: "black",
						//labelOffset: 0,
						radius: 1000,
						labels: false
						//labelStyle: "outside"
					}).addSeries("Series A", [{
							y: TongSoLuong - BiAnhHuong,
							text: TieuDe + "không ảnh hưởng",
							stroke: "black",
							color : "blue",
							tooltip: TieuDe + "không ảnh hưởng: " + Number((((TongSoLuong - BiAnhHuong) / TongSoLuong) * 100).toFixed(2)) + '%'
						},
						{
							y: BiAnhHuong,
							text: TieuDe + "bị tác động",
							stroke: "black",
							color : "red",
							tooltip: TieuDe + "bị tác động: " + Number(((BiAnhHuong / TongSoLuong) * 100).toFixed(2)) + '%'
						}
					]);
				var anim_a = new MoveSlice(chartTwo, "default");
				var anim_b = new Highlight(chartTwo, "default");
				var anim_c = new Tooltip(chartTwo, "default");
				chartTwo.render();
				var legendTwo = new Legend({
					chart: chartTwo
				}, that1.ChuThich);			
			},
			
			VeBieuDoTramDien: function(TramDGV,TramCGV,TramDXLX){
				that1.chartTwo.innerHTML = "";
				var chartTwo = new Chart(that1.chartTwo);
				chartTwo.setTheme(MiamiNice)
					.addPlot("default", {
						type: Pie,
						font: "normal normal 11pt Tahoma",
						fontColor: "black",
						//labelOffset: 0,
						radius: 1000,
						labels: false
						//labelStyle: "outside"
					}).addSeries("Series A", [{
							y: TramDGV,
							text: "Đã giao việc",
							stroke: "black",
							color : "yellow",
							tooltip: "Trạm đã giao việc: " + Number(((TramDGV / (TramDGV + TramCGV + TramDXLX)) * 100).toFixed(2)) + '%'
						},
						{
							y: TramCGV,
							text: "Hủy việc",
							stroke: "black",
							color : "orange",
							tooltip: "Trạm hủy việc: " + Number(((TramCGV / (TramDGV + TramCGV + TramDXLX)) * 100).toFixed(2)) + '%'
						},
						{
							y: TramDXLX,
							text: "Xử lý xong",
							stroke: "black",
							color : "blue",
							tooltip: "Xử lý xong: " + Number(((TramDXLX / (TramDGV + TramCGV + TramDXLX)) * 100).toFixed(2)) + '%'
						}
					]).connectToPlot("default", function (evt) {
                        // React to mouseover event
                        if (evt.type == "onclick") {
                            that1.VeDiemGiaoViecLenBanDo(evt.index);
                        }
                    });
				var anim_a = new MoveSlice(chartTwo, "default");
				var anim_b = new Highlight(chartTwo, "default");
				var anim_c = new Tooltip(chartTwo, "default");
				chartTwo.render();
				var legendTwo = new Legend({
					chart: chartTwo
				}, that1.legendTwo);
			},
			
			VeDiemGiaoViecLenBanDo: function(chiSo) {
				
				// hien giao dien giam sat giao viec cho cac tram bi su co (o can nua)
				//that1.cmdBack.style.display="";
				//that1.BaoCaoTongTrang.style.display="none";	

				// bat gia tri trang thai liet ke diem giao viec tren ban do
				that1.KiemTraTrangThaiLietKeTramSuCoTrenBanDo = 1;
				that1.KiemTraTrangThaiLietKeDiemGiaoViecTrenBanDo = 1;				
				
				if (chiSo == 2) {
					//alert("xu ly xong");
					that1.ThemTramDaGiaoViecTrenBanDo(that1.MAPS[0],that1.PointTramDaXuLyXong,'blue');
				} else if (chiSo == 0) {
					//alert("da giao viec");
					that1.ThemTramDaGiaoViecTrenBanDo(that1.MAPS[0],that1.PointTramDaGiaoViec,'yellow');
				} else if(chiSo == 1){
					//alert("chua giao viec");
					that1.ThemTramDaGiaoViecTrenBanDo(that1.MAPS[0],that1.PointTramChuaGiaoViec,'orange');
				}else{
				}

			},
			
			LayDanhSachNhanCong: function(){

				var query = new Query();
					query.orderByFields = ["name"];
					query.outFields = ["*"];
					query.returnGeometry = false;
					query.where = "1=1";
				var task = new QueryTask("https://arcgis.esrivn.net/server/rest/services/Hosted/workers_40213101d7f748acaaeb0db9605c5813/FeatureServer/0");
					task.execute(query, function(set){
						
						// dua ve gia tri mac dinh ban dau
						that1.GiamSatNhanCong.innerHTML = "";
						that1.cboNhanCong.innerHTML = "";
						that1.cboNhanCong.innerHTML = "<option value='all'>--chọn nhân công--</option>";
						
						for (var i = 0; i < set.features.length; i++) {
							var attr = set.features[i].attributes;
							// tao danh sach nhan cong
							var opt = document.createElement("option");
								opt.value = attr["objectid"];
								opt.innerHTML = attr["name"];
							that1.cboNhanCong.add(opt);
							var trangThaiLamViec = "";
							if(attr["status"] == 0){
								trangThaiLamViec = "làm việc";
							}else if(attr["status"] == 1){
								trangThaiLamViec = "nghỉ";
							}else if(attr["status"] == 2){
								trangThaiLamViec = "tạm nghỉ";
							}else{
								trangThaiLamViec = "khác";
							}
							var row=that1.GiamSatNhanCong.insertRow();
								row.innerHTML="<td>" + attr["name"] + "</td><td>0</td><td>" + trangThaiLamViec + "</td>";
								row.align="center";
								row.tag = attr["objectid"];
								// check so luong giao viec cho nhan cong
								var query1 = new Query();
									//query1.orderByFields = ["name"];
									query1.outFields = ["*"];
									query1.returnGeometry = false;
									query1.where = "workerid = " + attr["objectid"];
								var task1 = new QueryTask("https://arcgis.esrivn.net/server/rest/services/Hosted/assignments_40213101d7f748acaaeb0db9605c5813/FeatureServer/0");
									task1.execute(query1, function(set1){
										if(set1.features.length > 0){
											var attr = set1.features[0].attributes;
											for (var i = 0; i < that1.GiamSatNhanCong.rows.length; i++) {
												if(that1.GiamSatNhanCong.rows[i].tag == attr["workerid"]){
													that1.GiamSatNhanCong.rows[i].cells[1].innerText = set1.features.length;
												}
											}
										}
									}, function(err) {
										//alert(err.stack);
									});
						}
					}, function(err) {
						//alert(err.stack);
					});	
			},
			
			flashGraphic: function(graphic) {
				setInterval(dojo.partial(function (animateMe) {
					var shape = animateMe.getDojoShape();
					dojox_gfx_fx.animateStroke({
						shape: shape,
						duration: 1000,
						color: {
							start: that1.mauHienThi,
							end: esri.Color.fromString("rgb(255,0,0)")
						},
						width: {
							start: 10,
							end: 1
						}
					}).play();
				}, graphic), 500);
			},
			
			KiemTraTramSuCo: function(){
				
				// dua cac tham so ve bieu do ve dang mac dinh
				that1.SoLuongTramSuCo = 0;

				var urlServices = "https://arcgis.esrivn.net/server/rest/services/UN_Solution/B%E1%BA%A3n_%C4%90%E1%BB%93_M%E1%BA%A1ng_L%C6%B0%E1%BB%9Bi_HT/FeatureServer/4";

				var query = new Query();
				query.outFields = ["*"];
				query.orderByFields = ["NhietDo DESC"];
				query.returnGeometry = true;
				query.where = 'NhietDo > 80';

				var task = new QueryTask(urlServices);
				task.execute(query, function(set) {
					
					if(that1.KiemTraTrangThaiLietKeTramSuCoTrenBanDo == 0){
						// set tham so de zoom to toi tram su co
						that1.TramDaGiaoViecBD = set.features;
						if (that1.MAPS.length > 0) {
							that1.ThemTramDaGiaoViecTrenBanDo(that1.MAPS[0],that1.TramDaGiaoViecBD, 'red');
						}
						that1.tblT1.innerHTML = "";
						// dem so tram su co
						that1.SoLuongTramSuCo = set.features.length;
						for (var i = 0; i < set.features.length; i++) {
							var attr = set.features[i].attributes;
							// set tham so de zoom to toi tram su co
							that1.ZoomTramSuCo[attr["MaTram"]] = set.features[i].geometry;
							var row = that1.tblT1.insertRow();
							row.className = 'line';
							row.innerHTML = "<td>" + attr["MaTram"] + "</td><td>" + attr["NhietDo"] + "°C</td><td>" + attr["CongSuat"] + "</td>";
							//row.onclick=that1.row_onclick;
							row.onclick = that1.DieuHuongSuKien;
							row.tag = attr["MaTram"];
							row.align = "center";
							row.cells[0].tag = attr["MaTram"];
							row.cells[1].tag = attr["NhietDo"];
							row.cells[2].tag = attr["CongSuat"];
						}
						if (that1.nhietDo.lastElementChild.innerText != " ↑") {
							that1.sortTram(1, that1.nhietDo);
						}
						// tao menu phai cho bang TramSuCo
						that1.TaoMenuRightClick('TramSuCo');
						// bieu do tram dien su co
						//if((that1.SoLuongTramSuCo != 0)&&(that1.BieuDoTramDien.style.display == '')){
						//	that1.VeBieuDoTramDien(that1.SoLuongTramSuCo - (that1.SoLuongTramDaGiaoViec + that1.SoLuongTramChuaGiaoViec),that1.SoLuongTramDaGiaoViec,that1.SoLuongTramChuaGiaoViec);
						//}
						that1.txtSoLuongTramSuCo.innerHTML = "(" + that1.SoLuongTramSuCo + ") TRẠM ĐIỆN ĐANG BỊ SỰ CỐ";
					}
					
				}, function(err) {
					//alert(err.stack);
				});					

			},
			
			test: function(abc){
				var xyz = abc;
			},
			
			ThemTramDaGiaoViecTrenBanDo: function(banDo,TapDiemCanDanhDau,colorHienThi){
				
				// them diem tram da giao viec len ban do	
				// clear the graphics layer
				banDo.graphics.clear();
				// polygon symbol for drawing results
				var sfsResultPoint = new SimpleMarkerSymbol();
				//sfsResultPoint.setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 255, 0, 0.5]), 1));
				sfsResultPoint.setOutline(null);
				sfsResultPoint.setColor(new Color([0,0,139,0.5]));
				/*
				 * Step: Extract the array of features from the results
				 */
				var arrayFeatures = TapDiemCanDanhDau;
				// loop through results
				array.forEach(arrayFeatures, function (feature) {

					/*
					 * Step: Symbolize and add each graphic to the map's graphics layer
					 */
					feature.setSymbol(sfsResultPoint);
					banDo.graphics.add(feature);
					// gan mau hien thi cho flashGraphic
					that1.mauHienThi = colorHienThi;
					that1.flashGraphic(feature);

				});
				
				// update the map extent
				var extentViewshed = graphicsUtils.graphicsExtent(banDo.graphics.graphics);
				banDo.setExtent(extentViewshed, true);
				
			},

			DieuHuongSuKien: function(){
				if(that1.TramDaGiaoViecBD != 0){
					if(that1.KiemTraTrangThaiLietKeTramSuCoTrenBanDo == 1){
						that1.ThemTramDaGiaoViecTrenBanDo(that1.MAPS[0],that1.TramDaGiaoViecBD, 'red');
					}else{
						that1.ZoomToTramSuCo(0,this.tag);
					}
				}
				// thiet lap trang thai san sang cua viec ve diem tren ban do
				that1.KiemTraTrangThaiLietKeTramSuCoTrenBanDo = 0;
				that1.KiemTraTrangThaiLietKeDiemGiaoViecTrenBanDo = 1;
			},
			
			ZoomToTramSuCo: function(chiSoDieuKhien, maTram){	
				/*
				alert(this.tag);
				var urlServices = "https://arcgis.esrivn.net/server/rest/services/UN_Solution/B%E1%BA%A3n_%C4%90%E1%BB%93_M%E1%BA%A1ng_L%C6%B0%E1%BB%9Bi_HT/FeatureServer/4";
				var query = new Query();
					query.outFields = ["*"];
					//query.orderByFields=["NhietDo DESC"];
					query.returnGeometry = true;
					query.where = "OBJECTID = " + this.tag;
				var task = new QueryTask(urlServices);
					task.execute(query, function(set) {
						// update the map extent
						//var extentViewshed = graphicsUtils.graphicsExtent(set.features[0].geometry);
						//that1.MAPS[0].setExtent(extentViewshed, true);
						that1.MAPS[0].centerAndZoom(set.features[0].geometry,19);
					}, function(err) {
						alert(err.stack);
					});
				*/	
				tapDiemCanHienThi = 0;
				if(chiSoDieuKhien == 0){
					tapDiemCanHienThi = that1.ZoomTramSuCo;
				}else if(chiSoDieuKhien == 1){
					tapDiemCanHienThi = that1.ZoomTramGiaoViec;
				}else{
				}
				that1.MAPS[0].centerAndZoom(tapDiemCanHienThi[maTram],19);
			},
			
			HienDiemGiaoViecTrenBanDo: function(){
				//alert("ok giao viec");
				if(that1.DanhSachTramTrongBangGV != 0){
					if(that1.KiemTraTrangThaiLietKeDiemGiaoViecTrenBanDo == 1){
						that1.ThemTramDaGiaoViecTrenBanDo(that1.MAPS[0],that1.DanhSachTramTrongBangGV,'green');
					}else{
						that1.ZoomToTramSuCo(1,this.tag);
					}
				}
				
				// hien giao dien giam sat giao viec cho cac tram bi su co
				//that1.cmdBack.style.display="";
				//that1.BieuDoTramDien.style.display="";
				//that1.BaoCaoTongTrang.style.display="none";	
				//that1.BaoCaoChiTietTram.style.display="none";	
				//that1.BaoCaoGiaoViec.style.display="none";	
				// tat gia tri trang thai liet ke diem giao viec tren ban do
				that1.KiemTraTrangThaiLietKeTramSuCoTrenBanDo = 1;
				that1.KiemTraTrangThaiLietKeDiemGiaoViecTrenBanDo = 0;
				// bieu do tram dien su co
				if(that1.BieuDoTramDien.style.display == ''){
					that1.VeBieuDoTramDien(that1.SoLuongTramDaGiaoViec,that1.SoLuongTramChuaGiaoViec,that1.SoLuongTramDaXuLyXong);
				}
			},
			
			KiemTraTrangThaiGiaoViec: function(){
				
					var urlServices2 = "https://arcgis.esrivn.net/server/rest/services/Hosted/assignments_40213101d7f748acaaeb0db9605c5813/FeatureServer/0";
					var tramDaXuLy = 0;
					var tramChuaXuLy = 0;
					
					var query2 = new Query();
						query2.outFields = ["*"];
						query2.orderByFields=["created_date DESC"];
						query2.returnGeometry = true;
						query2.where = "1=1";
					
					var task2 = new QueryTask(urlServices2);
						task2.execute(query2, function(set) {
							// dua cac tham so ve bieu do ve gia tri mac dinh
							that1.SoLuongTramDaGiaoViec = 0;
							that1.SoLuongTramChuaGiaoViec = 0;
							that1.SoLuongTramDaXuLyXong= 0;
							that1.DanhSachTramTrongBangGV = 0;
							that1.PointTramDaXuLyXong = [];
							that1.PointTramDaGiaoViec = [];
							that1.PointTramChuaGiaoViec = [];
							//that1.TramDaGiaoViecBD = set.features;	
							that1.DanhSachTramTrongBangGV = set.features;
							that1.TramDaGiaoViec.innerHTML = "";
							var x = 0,y = 0, z = 0;
							for(var i=0;i<set.features.length;i++){
								var attr=set.features[i].attributes;
								// set tham so de zoom to toi tram giao viec
								that1.ZoomTramGiaoViec[attr["notes"]] = set.features[i].geometry;
								var row=that1.TramDaGiaoViec.insertRow();
								if(attr["status"] == 0){
									that1.SoLuongTramChuaGiaoViec = that1.SoLuongTramChuaGiaoViec + 1;
									that1.PointTramChuaGiaoViec[x] = set.features[i];
									x = x + 1;
								}else if(attr["status"] == 1){
									that1.SoLuongTramDaGiaoViec = that1.SoLuongTramDaGiaoViec + 1;
									that1.PointTramDaGiaoViec[y] = set.features[i];
									y = y + 1;
								}else if(attr["status"] == 2){
									that1.SoLuongTramDaXuLyXong = that1.SoLuongTramDaXuLyXong + 1;
									that1.PointTramDaXuLyXong[z] = set.features[i];
									z = z + 1;
								}else{
									// trang thai khac
								}
								row.className='line';
								var trangThaiCongViec = "";
								if(attr["status"] == 0){
									trangThaiCongViec = "hủy việc";
								}else if(attr["status"] == 1){
									trangThaiCongViec = "đã giao";
								}else if(attr["status"] == 2){
									trangThaiCongViec = "xử lý xong";
								}
								else{
									trangThaiCongViec = "khác";
								}
								var ngayKhoiTao = new Date(attr["created_date"]);
								row.innerHTML="<td>" + ngayKhoiTao.getFullYear() + ":"  + (ngayKhoiTao.getMonth()+1) + ":" + ngayKhoiTao.getDate() + " " + ngayKhoiTao.getHours() + ":" + ngayKhoiTao.getMinutes() + "</td><td>" + attr["notes"] + "</td><td>" + trangThaiCongViec + "</td>";
								row.onclick=that1.HienDiemGiaoViecTrenBanDo;
								row.tag = attr["notes"];
								row.align="center";	
							}
							// tao menu phai cho bang GiaoViecTramSuCo
							that1.TaoMenuRightClick('GiaoViecTramSuCo');						
							// bieu do tram dien su co
							if(that1.BieuDoTramDien.style.display == ''){
								that1.VeBieuDoTramDien(that1.SoLuongTramDaGiaoViec,that1.SoLuongTramChuaGiaoViec,that1.SoLuongTramDaXuLyXong);
							}	
						}, function(err) {
							//alert(err.stack);
						});	
					
			},
			
			KiemTraTramDien: function(){
				//console.log('Hello anh em every 1 seconds')
				if(that1.KiemTraTrangThaiLietKeTramSuCoTrenBanDo == 0){
					// cac thong tin can duoc cap nhat thuong xuyen
					that1.KiemTraTrangThaiGiaoViec();
					that1.KiemTraTramSuCo();
					// lay danh sach nhan cong
					//that1.LayDanhSachNhanCong();
					//that1.LayDanhSachCar();
				}
			},
			
			TaoMenuRightClick: function(tenBangCanTaoMenu){
				// tao menu right click
				var menu = new Menu({
					targetNodeIds: [tenBangCanTaoMenu],
					selector: "tr.line"
				});
				menu.addChild(new MenuItem({
					label: "Xử lý sự cố - Workforce",
					onClick: function (evt) {
						var node = this.getParent().currentTarget;
						//alert(node.tag);
						that1.GiaoViec(node.tag);
						that1.HienPopupGiaoViec();
					}
				}));
				menu.addChild(new MenuItem({
					label: "Khách hàng ảnh hưởng",
					onClick: function (evt) {
						var node = this.getParent().currentTarget;
						//alert(node.tag);
						that1.row_onclick(node.tag);
						that1.HienPopupTimPhamViAnhHuong();
					}
				}));
			},
			
			HienPopupTimPhamViAnhHuong: function(){
				that1.MyDialog = new Dialog({
				title: "Phạm vi ảnh hưởng",
				content: that1.PopupTimPhamViAnhHuong,
				style: "width:400mm"
				});
				that1.MyDialog.show();
			},
			
			HienPopupGiaoViec: function(){
				that1.MyDialog = new Dialog({
				title: "Giao việc",
				content: that1.PopupGiaoViec,
				style: "width:95mm"
				});
				//that1.BaoCaoGiaoViec.style.display = "";
				//DialogGiaoViec.set("content",that1.BaoCaoGiaoViec);
				//DialogGiaoViec.set("content",that1.BaoCaoChiTietTram,that1.BaoCaoGiaoViec);
				that1.MyDialog.show();
			},
			
			// xap sep tram
			sortTram:function(index,col,isStr){
				for(var i=0;i<col.parentNode.children.length;i++)
					col.parentNode.children[i].lastElementChild.innerHTML=" ↑↓";
					col.lastElementChild.innerHTML=(col.isAsc)?" ↓":" ↑";
				
					var table=col.parentNode.parentNode.nextElementSibling;
					var ok=true;
					while(ok){
						ok=false;
						for(var i=1;i<table.rows.length;i++){
							var vali=(isStr)?table.rows[i].cells[index].innerHTML:table.rows[i].cells[index].tag;
							var vali_1=(isStr)?table.rows[i-1].cells[index].innerHTML:table.rows[i-1].cells[index].tag;
							if((col.isAsc&&vali<vali_1)||(!col.isAsc&&vali>vali_1)){
								table.rows[i-1].parentNode.insertBefore(table.rows[i], table.rows[i-1]);
								ok=true;
							}
						}
				}
				col.isAsc=!col.isAsc;
				// danh lai so thu tu cho bang
				/*
				for(var i=0;i < table.childNodes.length;i++){
					table.childNodes[i].firstElementChild.innerHTML = i + 1 ;
				}
				*/
			},
			
			GiaoViec: function(maTram){
				
				// lam sach man hinh cua phan hien thi ban do (o can nua)
				//that1.MAPS[0].graphics.clear();
				
				//alert("giao viec");
				// thiet dat phan giao dien giao viec
				that1.BaoCaoChiTietTram.style.display="";
				//that1.BaoCaoChiTietTram.scrollIntoView();
				that1.cmdBack.style.display="";
				that1.TheoDoiNhanCong.style.display="";
				that1.BaoCaoGiaoViec.style.display="";
				//that1.BaoCaoTramDaGiaoViec.style.display="none";
				//that1.BaoCaoTongTrang.style.display="none";
				//that1.BieuDoTramDien.style.display="none";
				
				// dua gia tri cua thong tin tram ve null
						
				that1.txtObjectId.innerHTML =  "";
				that1.txtMaTram.innerHTML =  "";
				that1.txtLoaiTram.innerHTML =  "";
				that1.txtEnabled.innerHTML =  "";
				that1.txtAncillaryRole.innerHTML =  "";
				that1.txtCongXuat.innerHTML =  "";
				that1.txtNhietDo.innerHTML =  "";
				
				// bat gia tri trang thai liet ke diem giao viec tren ban do
				that1.KiemTraTrangThaiLietKeDiemGiaoViecTrenBanDo = 1;	
				
				var urlServices = "https://arcgis.esrivn.net/server/rest/services/UN_Solution/B%E1%BA%A3n_%C4%90%E1%BB%93_M%E1%BA%A1ng_L%C6%B0%E1%BB%9Bi_HT/FeatureServer/4";
				
				var query = new Query();
					query.outFields = ["*"];
					//query.orderByFields=["NhietDo DESC"];
					query.returnGeometry = true;
					query.where = "MaTram = '" + maTram + "'";
				var task = new QueryTask(urlServices);
					task.execute(query, function(set) {
						
						// danh dau tram dien dang giao viec (khong can nua)
						//that1.ThemTramDaGiaoViecTrenBanDo(that1.MAPS[0],set.features,'red');
						
						// thong tin tram
						that1.ToaDoX = set.features[0].geometry.x;
						that1.ToaDoY = set.features[0].geometry.y;
						var attrTram=set.features[0].attributes;
						that1.txtObjectId.innerHTML =  attrTram["OBJECTID"];
						that1.txtMaTram.innerHTML =  attrTram["MaTram"];
						that1.txtLoaiTram.innerHTML =  attrTram["LoaiTram"];
						that1.txtEnabled.innerHTML =  attrTram["Enabled"];
						that1.txtAncillaryRole.innerHTML =  attrTram["AncillaryRole"];
						that1.txtCongXuat.innerHTML =  attrTram["CongSuat"];
						that1.txtNhietDo.innerHTML =  attrTram["NhietDo"];
						
					}, function(err) {
						//alert(err.stack);
					});
			},
			XuLyGiaoViec: function(){
				var urlServices = "https://arcgis.esrivn.net/server/rest/services/Hosted/assignments_40213101d7f748acaaeb0db9605c5813/FeatureServer/0";
				var query = new Query();
					query.outFields = ["*"];
					//query.orderByFields=["NhietDo DESC"];
					query.returnGeometry = false;
					query.where = "notes = '" + that1.txtMaTram.innerHTML + "'";
				var task = new QueryTask(urlServices);
					task.execute(query, function(set) {
						if(set.features.length > 0){
							var attrGiaoViec=set.features[0].attributes;
							that1.CapNhatGiaoViec(attrGiaoViec["objectid"]);
						}else{
							that1.ThemMoiGiaoViec();
						}
					}, function(err) {
						//alert(err.stack);
					});						
			},
			CapNhatGiaoViec: function(IdGiaoViec){
				var trangThai = 0;
				var nhanCongId = null;
				if(that1.cboNhanCong.value != 'all'){
					trangThai = 1;
					nhanCongId = that1.cboNhanCong.value
				}
				//alert(that1.txtObjectId.textContent);
					var servicesUrl = "https://arcgis.esrivn.net/server/rest/services/Hosted/assignments_40213101d7f748acaaeb0db9605c5813/FeatureServer/0/updateFeatures";
                    var tramJson = [{
                        "attributes": {
							"objectid": IdGiaoViec,
                            "assignmenttype": that1.txtLoaiSuCo.value,
                            "status": trangThai,
							"workerid": nhanCongId
                        }
                    }]
					
					var request = esriRequest({
                        url: servicesUrl,
                        content: {
                            f:"json",
                            features: JSON.stringify(tramJson)
                        }
                    }, { usePost: true }).then(function (resp) {
                        //alert(resp)
						that1.KiemTraTrangThaiGiaoViec();
						// lay danh sach nhan cong
						that1.LayDanhSachNhanCong();
						alert("Cập nhật thông báo sự cố hoàn thành !")
                    },function(err){
                        alert(err)
                    });				
			},
			ThemMoiGiaoViec: function(){
				var trangThai = 0;
				var nhanCongId = null;
				if(that1.cboNhanCong.value != 'all'){
					trangThai = 1;
					nhanCongId = that1.cboNhanCong.value
				}
				//alert(that1.txtObjectId.textContent);
					var servicesUrl = "https://arcgis.esrivn.net/server/rest/services/Hosted/assignments_40213101d7f748acaaeb0db9605c5813/FeatureServer/0/addFeatures";
                    var tramJson = [{
                        "geometry": {
                            "x": that1.ToaDoX,
                            "y": that1.ToaDoY
                        },
                        "attributes": {
                            "assignmenttype": that1.txtLoaiSuCo.value,
                            "status": trangThai,
							"notes": that1.txtMaTram.textContent,
							"workerid": nhanCongId,
                            "location": "Trạm :" + that1.txtMaTram.textContent + " - đ/c : Quận 3 - TP Hồ Chí Minh",
                            "description": "Mã trạm: " + that1.txtMaTram.textContent
                        }
                    }]
					
					var request = esriRequest({
                        url: servicesUrl,
                        content: {
                            f:"json",
                            features: JSON.stringify(tramJson)
                        }
                    }, { usePost: true }).then(function (resp) {
                        //alert(resp)
						that1.KiemTraTrangThaiGiaoViec();
						// lay danh sach nhan cong
						that1.LayDanhSachNhanCong();
						alert("Thông báo sự cố đã được khởi tạo !")
                    },function(err){
                        alert(err)
                    });
					
			},
			
			LayBanDoMangLuoi: function(){
				/*
				document.getElementById(that1.map.id).innerHTML="";
				arcgisUtils.arcgisUrl = "http://arcgis.esrivn.net/portal/sharing/rest/content/items";
				arcgisUtils.createMap("b6e223a3343e413084b06aa0b45e0777", that1.map.id).then(function(resp){	
					that1.MAPS[0] = resp.map;
					if(that1.TramDaGiaoViecBD != 0){
						that1.ThemTramDaGiaoViecTrenBanDo(that1.MAPS[0],that1.TramDaGiaoViecBD,'red');
					}
				});
				*/
				that1.banDoMangLuoiDien.innerHTML = "";
				arcgisUtils.arcgisUrl = "http://arcgis.esrivn.net/portal/sharing/rest/content/items";
				arcgisUtils.createMap("b6e223a3343e413084b06aa0b45e0777", that1.banDoMangLuoiDien).then(function(resp){	
					that1.MAPS[1] = resp.map;
				});
				
			},
			
			row_onclick: function(maTram){

				// thiet dat giao dien phan tim pham vi anh huong cua su co
				that1.BaoCaoChiTietTramAH.style.display="";
				that1.BaoCaoChiTietTram.scrollIntoView();
				that1.cmdBackAH.style.display="";
				that1.PhamViAnhHuong.style.display="";
				that1.DanhSachKhachHang.style.display="";
				that1.BieuDoPhamViAnhHuong.style.display="";
				that1.banDoMangLuoiDien.style.display="";
				that1.BaoCaoGiaoViec.style.display="none";
				//that1.BaoCaoTongTrang.style.display="none";
				//that1.BaoCaoTramDaGiaoViec.style.display="none";
				//that1.BieuDoTramDien.style.display="none";
				that1.TheoDoiNhanCong.style.display="none";
				
				// dua gia tri cua thong tin tram ve null
						
				that1.txtObjectIdAH.innerHTML =  "";
				that1.txtMaTramAH.innerHTML =  "";
				that1.txtLoaiTramAH.innerHTML =  "";
				that1.txtEnabledAH.innerHTML =  "";
				that1.txtAncillaryRoleAH.innerHTML =  "";
				that1.txtCongXuatAH.innerHTML =  "";
				that1.txtNhietDoAH.innerHTML =  "";
				
				// thong tin ve pham vi anh huong cua tram su co ve null
				that1.txtKhachHang.innerHTML =  "";
				that1.txtTruDien.innerHTML =  "";
				that1.txtTBDC.innerHTML =  "";
				that1.txtTuyenDay.innerHTML =  "";
				
				// dua thong tin danh sach khach hang bi anh huong ve null
				that1.dsKhachHang.innerHTML = "";
				
				// clear the graphics layer
				if(that1.MAPS[1] != null){
					that1.MAPS[1].graphics.clear();
				}
				
				//alert(this.tag);
				var urlServices = "https://arcgis.esrivn.net/server/rest/services/UN_Solution/B%E1%BA%A3n_%C4%90%E1%BB%93_M%E1%BA%A1ng_L%C6%B0%E1%BB%9Bi_HT/FeatureServer/4";
				var gpViewshed = new Geoprocessor("https://arcgis.esrivn.net/server/rest/services/UN_Solution/TraceDownstream_UNSolution/GPServer/DemoUNSolution");
				var query = new Query();
					query.outFields = ["*"];
					//query.orderByFields=["NhietDo DESC"];
					query.returnGeometry = true;
					query.where = "MaTram = '" + maTram + "'";
				var task = new QueryTask(urlServices);
					task.execute(query, function(set) {
						
						
						
						// thong tin tram
						var attrTram=set.features[0].attributes;
						that1.txtObjectIdAH.innerHTML =  attrTram["OBJECTID"];
						that1.txtMaTramAH.innerHTML =  attrTram["MaTram"];
						that1.txtLoaiTramAH.innerHTML =  attrTram["LoaiTram"];
						that1.txtEnabledAH.innerHTML =  attrTram["Enabled"];
						that1.txtAncillaryRoleAH.innerHTML =  attrTram["AncillaryRole"];
						that1.txtCongXuatAH.innerHTML =  attrTram["CongSuat"];
						that1.txtNhietDoAH.innerHTML =  attrTram["NhietDo"];
						
						var fsInputPoint = new FeatureSet();
						fsInputPoint.features.push(set.features[0]);
						var params = {
							Flags: fsInputPoint
						};
						//gpViewshed.on("execute-complete", that1.displayViewshed);;
						gpViewshed.execute(params, function(results, messages) {
							//do something with the results
							//var features = results[1].value.features;
							
							// danh dau tram dien anh huong toi khach hang
							that1.ThemTramDaGiaoViecTrenBanDo(that1.MAPS[1],set.features,'red');
							
							// thong tin ve pham vi anh huong cua tram su co
							that1.txtKhachHang.innerHTML =  results[4].value.features.length;
							// bieu do pham vi khach hang bi anh huong
							that1.VeBieuDoPhamViAnhHuong(8926,results[4].value.features.length,'');
							that1.txtTruDien.innerHTML =  results[0].value.features.length;
							that1.txtTBDC.innerHTML =  results[2].value.features.length;
							that1.txtTuyenDay.innerHTML =  results[3].value.features.length;
							
							// polygon symbol for drawing results
							var sfsResultPoint = new SimpleMarkerSymbol();
							//sfsResultPoint.setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 255, 0, 0.5]), 1));
							sfsResultPoint.setOutline(null);
							sfsResultPoint.setColor(new Color([255, 127, 0, 0.5]));
							
							/*
							 * Step: Extract the array of features from the results
							 */
							//var pvResult = results.results[4];
							var pvResult = results[4];
							var gpFeatureRecordSetLayer = pvResult.value;
							var arrayFeatures = gpFeatureRecordSetLayer.features;
									
							/*hien thi danh sach khach hang bi anh huong*/							
							var i = 0;
							while (i < arrayFeatures.length) {
								var row1=that1.dsKhachHang.insertRow();
								row1.align="center";
								//row1.style="color: white";
								var j = 1;
								while ((i < arrayFeatures.length) && (j <= 3)) {
									attr = arrayFeatures[i].attributes;
									row1.innerHTML = row1.innerHTML + "<td style='width:30mm;'>" + attr["KH_ID"] + "</td><td style='width:30mm;'>" + attr["MaTram"] + "</td><td style='width:30mm;'>" + attr["Enabled"] + "</td>";
									i = i + 1;
									j = j + 1;
								}
							}
								
							// loop through results
							array.forEach(arrayFeatures, function (feature) {

								/*
								 * Step: Symbolize and add each graphic to the map's graphics layer
								 */
								feature.setSymbol(sfsResultPoint);
								that1.MAPS[1].graphics.add(feature);

							});
							
							// update the map extent
							var extentViewshed = graphicsUtils.graphicsExtent(that1.MAPS[1].graphics.graphics);
							that1.MAPS[1].setExtent(extentViewshed, true);
							
							
						}, function(ErrorInfo) {
							console.log(ErrorInfo.jobStatus);
						});
					}, function(err) {
						//alert(err.stack);
					});	
			},
			
			
			displayViewshed: function(results, messages) {

				// polygon symbol for drawing results
				var sfsResultPolygon = new SimpleFillSymbol();
				sfsResultPolygon.setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 255, 0, 0.5]), 1));
				sfsResultPolygon.setColor(new Color([255, 127, 0, 0.5]));

				/*
				 * Step: Extract the array of features from the results
				 */
				var pvResult = results.results[4];
				var gpFeatureRecordSetLayer = pvResult.value;
				var arrayFeatures = gpFeatureRecordSetLayer.features;

				// loop through results
				array.forEach(arrayFeatures, function (feature) {

					/*
					 * Step: Symbolize and add each graphic to the map's graphics layer
					 */
					feature.setSymbol(sfsResultPolygon);
					that1.MAPS[0].graphics.add(feature);

				});

				// update the map extent
				//var extentViewshed = graphicsUtils.graphicsExtent(that1.MAPS[0].graphics.graphics);
				//that1.MAPS[0].setExtent(extentViewshed, true);
			},
			
			QuayTroLai:function(){
				
				// tat dialog
				that1.MyDialog.hide();
				//that1.DialogPhamViAnhHuong.hide();
				
				// lam sach man hinh cua phan hien thi ban do (o can nua)
				//that1.MAPS[0].graphics.clear();
				
				// thiet dat lai giao dien ban dau - theo doi cac tram su co va giao viec
				that1.BaoCaoTongTrang.style.display="";
				//that1.BaoCaoTongTrang.scrollIntoView();
				that1.BaoCaoTramDaGiaoViec.style.display="";
				that1.BieuDoTramDien.style.display="";
				that1.cmdBack.style.display="none";
				that1.BaoCaoChiTietTram.style.display="none";
				that1.BaoCaoGiaoViec.style.display="none";
				that1.PhamViAnhHuong.style.display="none";
				that1.DanhSachKhachHang.style.display="none";
				that1.BieuDoPhamViAnhHuong.style.display="none";
				that1.TheoDoiNhanCong.style.display="none";
				that1.BaoCaoChiTietTramAH.style.display="none";
				that1.cmdBackAH.style.display="none";
				that1.banDoMangLuoiDien.style.display="none";
				
				// bat gia tri trang thai liet ke diem giao viec tren ban do
				that1.KiemTraTrangThaiLietKeDiemGiaoViecTrenBanDo = 1;
				
				// cac thong tin can duoc cap nhat thuong xuyen
				that1.KiemTraTrangThaiGiaoViec();
				that1.KiemTraTramSuCo();
				// lay danh sach nhan cong
				that1.LayDanhSachNhanCong();
					
			},
			
            startup: function() {
                this.inherited(arguments);
                this.mapIdNode.innerHTML = "map id:" + this.map.id;
                console.log("startup")
            },
            onOpen: function() {
               // console.log("onOpen")
			   /*
				var p = this.getPanel();
				p.setPosition({
					left: 0,
					top: 0,
					//right: 0,
					//bottom: 0,
					width: "100%",
					height: "100%",
					zIndex: 99
				});	
				*/				
            },
            onClose: function() {
                console.log("onClose")
            },
            onMinimize: function() {
                console.log("onMinimize")
            },
            onMaximize: function() {
                console.log("onMaximize")
            },
            onSignIn: function(a) {
                console.log("onSignIn")
            },
            onSignOut: function() {
                console.log("onSignOut")
            },
            showVertexCount: function(a) {
                this.vertexCount.innerHTML = "The vertex count is: " + a
            }
        })
    });